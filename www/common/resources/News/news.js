angular.module('ep.resources.news', [
	'ngResource'
])
.factory('News', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/news/',
		{
			id: "@id"
		},
	    {
	        GetByFamily: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/news/get-by-family/:id'
	        },
	        GetByTeacher: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/cms/news/get-by-teacher/:id'
	        }
	    }
    );

	return resource;
}]);