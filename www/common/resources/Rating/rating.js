angular.module('ep.resources.rating', [
	'ngResource'
])
.factory('Rating', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/event/',
		{
			id: "@id"
		},
	    {
	        GetRatingByChild: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/rating/get-by-child/:id_child/:id_child_group'
	        },
	        GetRByChildPeriodGroup: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/rating/get-by-chid-group-period/:id_child/:id_period/:id_child_group'
	        },
	        GetRatingByCurrentYear: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/rating/get-by-child-curr-year/:id_child/:id_child_group'
	        },
	    }
    );


	return resource;
}]);