angular.module('ep.resources.family', [
	'ngResource'
])
.factory('Family', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/family/',
		{
			id: "@id"
		},
	    {
	       	CreateUser: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/app/family/createUser'
	        },
	        Login: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/app/family/login'
	        },
	        Logout: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/family/logout'
	        },
	       	AuthCheck: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/family/authCheck'
	        },
	       	SaveDeviceToken: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/app/family/save-device-token'
	        },
	        SaveDeviceTokenT: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/cms/family/save-device-token'
	        },
	       	SetNotif: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/app/family/set-notif'
	        },
	       	GetNotif: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/app/family/get-notif'
	        },
	       	getToken: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/family/getToken'
	        },
	       	SetNotifT: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/cms/family/set-notif'
	        },
	       	GetNotifT: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/cms/family/get-notif'
	        },
	       	getTokenT: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/cms/family/getToken'
	        }
	    }
    );

	return resource;
}]);