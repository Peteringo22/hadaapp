angular.module('ep.resources.school', [
	'ngResource'
])
.factory('School', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/school/',
		{
			id: "@id"
		},
	    {
	        GetSchool: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/school/get-school'
	        }
	    }
    );

	return resource;
}]);