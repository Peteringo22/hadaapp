angular.module('ep.resources.event', [
	'ngResource'
])
.factory('Event', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/event/',
		{
		},
	    {
	        getDateEvents: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/event/getNumberEvents'
	        },
	        GetByFamily: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/event/get-by-family/:id'
	        },
	        getEventsByDay: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/event/getEventsByDay/:date/:id'
	        },
	        getById: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/event/show/:id'
	        },
	        getByTeacher: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/cms/event/get-by-teacher'
	        },
	        getEventsByDayT: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/cms/event/get-by-day-t/:date'
	        }
	    }
    );

	return resource;
}]);