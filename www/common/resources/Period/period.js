angular.module('ep.resources.period', [
	'ngResource'
])
.factory('Period', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/period/',
		{
			id: "@id"
		},
	    {
	       	GetAll: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/period/get-all'
	        },
	        GetAllCurrentYear: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/period/get-all-year'
	        }
	    }
    );

	return resource;
}]);