angular.module('ep.resources.faq', [
	'ngResource'
])
.factory('Faq', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/faq/',
		{
			id: "@id"
		},
	    {
	        GetAll: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/faq/get-all'
	        }
	    }
    );

	return resource;
}]);