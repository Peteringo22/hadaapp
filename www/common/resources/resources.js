angular.module( 'ep.resources', [
    'ep.resources.family',
    'ep.resources.event',
    'ep.resources.teacher',
    'ep.resources.news',
    'ep.resources.faq',
    'ep.resources.school',
    'ep.resources.group',
    'ep.resources.rating',
    'ep.resources.period'
])

.factory('ApiConfig', function() {
    var apiConfig = {
        constants: {
        // baseUrl: "http://10.7.8.133/schoolpocket-back",
           //baseUrl: "https://ssl.hadaescolar.com/1si/core"
            //baseUrl: "https://ssl.hadaescolar.com/gabrielamistral/core"
            //baseUrl: "https://ssl.hadaescolar.com.mx/GMBackend"
            //anterior
            //baseUrl: "https://hadacomunica.com/laravel-backend-demo"
            baseUrl: "https://hadacomunica.com/gmback/laravel-backend"
        }
    };
 
    apiConfig.get = function() {
        return apiConfig.constants;
    };

    return apiConfig;
});