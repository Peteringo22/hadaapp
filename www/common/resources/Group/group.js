angular.module('ep.resources.group', [
	'ngResource'
])
.factory('Group', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/group/',
		{
			id: "@id"
		},
	    {
	       	getGroupsByChild: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/group/get-by-child-id/:id_child'
	        }
	    }
    );

	return resource;
}]);