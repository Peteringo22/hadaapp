angular.module('ep.resources.teacher', [
	'ngResource'
])
.factory('Teacher', ['$resource', 'ApiConfig', function($resource, apiConfig) {

	var config = apiConfig.get();

	var resource = $resource(
		config.baseUrl + '/public/index.php/event/',
		{
			id: "@id"
		},
	    {
	        GetTeacherByChild: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/teacher/get-by-child/:id'
	        },
	        GetProfileTeacher: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/app/teacher/get-profile/:id'
	        },
	        Login: {
	        	method:'POST',
	        	url: config.baseUrl + '/public/index.php/cms/admin/login'
	        },
	        AuthCheck: {
	        	method:'GET',
	        	url: config.baseUrl + '/public/index.php/cms/admin/authCheck'
	        }
	    }
    );

	return resource;
}]);