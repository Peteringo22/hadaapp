angular.module('ep.service.Auth',[
])
.factory("Auth", ['$q', '$timeout', '$rootScope', '$localStorage','$state','$stateParams','Family','$ionicPlatform','$http', 'Teacher', function($q, $timeout, $rootScope, $localStorage, $state,$stateParams, Family, $ionicPlatform, $http, Teacher){


	var Auth = {};

	function urlBase64Decode(str) {
        var output = str.replace('-', '+').replace('_', '/');
            switch (output.length % 4) {
               case 0:
                   break;
               case 2:
                   output += '==';
                   break;
               case 3:
                   output += '=';
                   break;
               default:
                   throw 'Illegal base64url string!';
            }
            return window.atob(output);
        }

    function getClaimsFromToken() {
        var token = $localStorage.token;
        var user = {};
        if (typeof token !== 'undefined') {
            var encoded = token.split('.')[1];
            user = JSON.parse(urlBase64Decode(encoded));
        }
        return user;
    }

   // var tokenClaims = getClaimsFromToken();
   // console.log(tokenClaims);

	Auth.checkNotif = function(){

		if($localStorage.role == "family"){
			Family.GetNotif({'token': $localStorage.deviceToken}).$promise.then(function(data){
				$localStorage.notif = data.notif;
	        });
		}
		else if($localStorage.role == "teacher"){
			Family.GetNotifT({'token': $localStorage.deviceToken}).$promise.then(function(data){
				$localStorage.notif = data.notif;
	        });
		}
	};


	Auth.login = function(query){
        var self = Auth, data = [], deferred = $q.defer();
        Family.Login(query, function (data) {
			if (data.success)
			{				
				$localStorage.token = data.token;
				var token = $localStorage.token;
           		var tokenClaims = getClaimsFromToken();
				$localStorage.id 	 = data.family.id;
				$localStorage.role = "family";
												 
				Family.SaveDeviceToken({token:$localStorage.deviceToken, type:$localStorage.deviceType, id_family:$localStorage.id}).$promise.then(function(data){
					Auth.checkNotif();
				});

				$localStorage.last_name = data.family.last_name;
				$localStorage.child = data.child;
				$localStorage.phone	= data.phone;
				if(data.child.length === 1)
				{
					$localStorage.selectedChild_id = $localStorage.child[0].id;
					$localStorage.selectedChild_photo = $localStorage.child[0].photo;
					$rootScope.data.selectedChild_id=$localStorage.selectedChild_id;
			    	$rootScope.data.selectedChild_photo=$localStorage.selectedChild_photo;
					$localStorage.oneChild = true;
					$state.transitionTo("app.aviso", $stateParams, {
					    reload: true,
					    inherit: false,
					    notify: true
					});
				}
				else
				{
					$localStorage.oneChild = false;
					$state.transitionTo("select-child", $stateParams, {
					    reload: true,
					    inherit: false,
					    notify: true
					});
				}
				deferred.resolve(data);
			}
			else
			{
				var teacher_login = {
					user: query.key,
					password: query.password
				};

				Teacher.Login(teacher_login, function (data) {

					if (data.success){

						console.log(data);

						Family.SaveDeviceTokenT({token:$localStorage.deviceToken, type:$localStorage.deviceType, id_family:-1}).$promise.then(function(data){
							Auth.checkNotif();
						});

						$localStorage.token = data.token;
						$localStorage.role = "teacher";
						if(data.admin.id_teacher == null)
							$localStorage.id_teacher = 0;
						else
							$localStorage.id_teacher = data.admin.id_teacher;

						$state.transitionTo("app.aviso", $stateParams, {
						    reload: true,
						    inherit: false,
						    notify: true
						});

					}
					else{

						$localStorage.id 	 = null;
						$localStorage.last_name = null;
						$localStorage.child = null;
						$localStorage.selectedChild_id = null;
						$localStorage.selectedChild_photo = null;
						$rootScope.data.child=[];

					}

					deferred.resolve(data);
				});


				$localStorage.id 	 = null;
				$localStorage.last_name = null;
				$localStorage.child = null;
				$localStorage.selectedChild_id = null;
				$localStorage.selectedChild_photo = null;
				$rootScope.data.child=[];
			}
        });
        
       return deferred.promise;
    };



	Auth.isConnected = function() {
		Family.AuthCheck().$promise.then(function(data){
			if (data.success)
			{
				$localStorage.id = data.family.id;
				$localStorage.last_name = data.family.last_name;
				$localStorage.child = data.child;
				$localStorage.phone	= data.phone;

				if(data.child.length === 1)
				{
					$localStorage.oneChild = true;
				}

				if(angular.isUndefined($rootScope.target))
				{
					$rootScope.target= 'aviso';
					$rootScope.targetParams ={'type':'hijo'};
				}

				Auth.checkNotif();
				$state.transitionTo($rootScope.target, $rootScope.targetParams, {
				    reload: true,
				    inherit: false,
				    notify: true
				});
			}
			else
			{
				Teacher.AuthCheck().$promise.then(function(datat){
					if(datat.success){
						$localStorage.role = "teacher";
						if(datat.admin.id_teacher == null)
							$localStorage.id_teacher = 0;
						else
							$localStorage.id_teacher = datat.admin.id_teacher;

						$state.transitionTo("app.aviso", $stateParams, {
						    reload: true,
						    inherit: false,
						    notify: true
						});
					}
					else
					{
			    		$localStorage.id 	 = null;
						$localStorage.last_name = null;
						$localStorage.child = null;
						$localStorage.selectedChild_id = null;
						$localStorage.selectedChild_photo = null;
						$rootScope.data.child=[];
						$state.transitionTo("home", null, {
						    reload: true,
						    inherit: false,
						    notify: true
						});
					}
				});
			}
	    });
	};

	Auth.logout = function(){
 		Family.Logout().$promise.then(function(dataLogout){
    		$localStorage.id 	 = null;
			$localStorage.last_name = null;
			$localStorage.child = null;
			$localStorage.selectedChild_id = null;
			$localStorage.selectedChild_photo = null;
			$rootScope.data.child=[];
			$state.transitionTo("home", $stateParams, {
			    reload: true,
			    inherit: false,
			    notify: true
			});
	    });
	    return $localStorage.connected;
    };    
    return Auth;
}]);






