// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('ep', [
    'ionic',
    'ep.main',
    'ep.preview',
    'ep.home',
    'ep.select-child',
    'ep.aviso',
    'ep.calendar',
    'ep.list-teacher',
    'ep.teacher-profile',
    'ep.faq',
    'ep.school',
    'ep.rating',
    'ep.resources',
    'ep.service',
    'ngAnimate',
    'ui.bootstrap',
    'ngCordova',
    'ngStorage'])

.config( function ( $stateProvider, $httpProvider, $urlRouterProvider, $compileProvider, $ionicConfigProvider) {
    $urlRouterProvider.otherwise( 'preview' );
    $httpProvider.defaults.withCredentials = false;
    $ionicConfigProvider.navBar.alignTitle('center');


    $httpProvider.interceptors.push(function ($rootScope, $localStorage,  $q, $location){

    return {
            
            //http request show loading
            request: function(config) {
                config.headers = config.headers || {};
                if ($localStorage.token) {
                    config.headers.Authorization ='Bearer ' + $localStorage.token;
                }
                return config;
            },
            
            //hide loading in case any occurred
            requestError: function(response) {
                $rootScope.$broadcast('HideLoader');
                return response;
            },
            
            //Hide loading once got response
            response: function(response) {
              // console.log('ERROR TOKEN');
              // console.log(response);
                $rootScope.$broadcast('HideLoader');
                if (response.error_token) {
                    $location.path('/home');
                }
                return response;
            },
            
            //Hide loading if got any response error 
            responseError: function(response) {
                $rootScope.$broadcast('HideLoader');
                if (response.error_token) {
                    $location.path('/home');
                }

                if (response.status === 500) {
                    // $location.path('/home');
                }
                return $q.reject(response);
            }
        }
        
    });



    
    $stateProvider.state( 'home', {
        url: '/home',
        templateUrl: "app/home/home.tpl.html",
        controller: 'HomeCtrl'
    })


    .state( 'preview', {
        url: '/preview',
        cache: false,
        templateUrl: "app/preview/preview.tpl.html",
        controller: 'PreviewCtrl'
    })

    .state( 'select-child', {
        url: '/select-child',
        templateUrl: "app/select-child/select-child.tpl.html",
        controller: 'SelectChildCtrl'
    })


    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: 'app/menu/menu.tpl.html',
        controller: 'MainController'
    })

    .state( 'app.aviso', {
        cache: false,
        url: '/aviso/:typeTab',
        views: {
            'main': {
                templateUrl: "app/aviso/aviso.tpl.html",
                controller: 'AvisoCtrl'
            }
        }
    })

    .state( 'app.calendar', {
        url: '/calendar/:currentDate',
        views: {
            'main': {
                templateUrl: "app/calendar/calendar.tpl.html",
                controller: 'CalendarCtrl'
            }
        },
        resolve:{
            EventDays:  function(Event,  $localStorage){ 
                var datos;
                if($localStorage.role == 'family'){

                    datos=Event.GetByFamily({'id':$localStorage.id}).$promise.then(function(data){
                        return data.days;
                    });
                    return datos;

                }
                else{
                    datos=Event.getByTeacher().$promise.then(function(data){
                        return data.days;
                    });
                    return datos;
                }
                
            }
        }
    })

    .state( 'app.list-teacher', {
        url: '/list-teacher',
        views: {
            'main': {
                templateUrl: "app/list-teacher/list-teacher.tpl.html",
                controller: 'ListTeacherCtrl'
            }
        }
    })

    .state( 'app.teacher-profile', {
        url: '/teacher-profile/:id',
        views: {
            'main': {
                templateUrl: "app/teacher-profile/teacher-profile.tpl.html",
                controller: 'TeacherProfileCtrl'
            }
        }
    })

    .state( 'app.faq', {
        url: '/faq',
        views: {
            'main': {
                templateUrl: "app/faq/faq.tpl.html",
                controller: 'FaqCtrl'
            }
        }
    })

    .state( 'app.rating', {
        url: '/rating',
        views: {
            'main': {
                templateUrl: "app/rating/rating.tpl.html",
                controller: 'RatingCtrl'
            }
        }
    })

    .state( 'app.school', {
        url: '/school',
        views: {
            'main': {
                templateUrl: "app/school/school.tpl.html",
                controller: 'SchoolCtrl'
            }
        }
    })


})



.run(function($state, $rootScope,ApiConfig, $location, $timeout, $ionicPlatform, $localStorage, $cordovaPush, $timeout, $cordovaLocalNotification, Family) {
    $rootScope.target= 'app.aviso';
    $rootScope.targetParams = null;
    $rootScope.paused=false;

    $rootScope.backImagesUrl=ApiConfig.get().baseUrl + '/public/images';
    $rootScope.backUrl=ApiConfig.get().baseUrl;
    $rootScope.data={
        myId:null,
        selectedChild_id:null,
        child:$localStorage.child,
        lastname:$localStorage.last_name
    };
    $rootScope.showAllChild = false;
    $rootScope.data.selectedChild_id=$localStorage.selectedChild_id;
    $rootScope.data.selectedChild_photo=$localStorage.selectedChild_photo;

    $ionicPlatform.ready(function() {

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
            //cordova.plugins.Keyboard.close();
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }


        document.addEventListener("resume", function (event) {
            $timeout(function() {
                $rootScope.paused=false;
            }, 2000);
        });

        document.addEventListener("pause", function (event) {
            $rootScope.paused=true;
        });

        $rootScope.target= 'app.aviso';
        $rootScope.targetParams = null;
        $rootScope.push = PushNotification.init({
            android: {
                senderID: "894795999857",
                //senderID: "185398695353",
                icon: "logo100",
                iconColor: "gray",
                forceShow : "true"
            },
            browser: {
                pushServiceURL: 'http://push.api.phonegap.com/v1/push'
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true",
                clearBadge: "true"
            },
            windows: {}
        });

        $rootScope.push.on('registration', function (data) {  
            $localStorage.testiTem = "asfsafas";
            $localStorage.deviceToken = data.registrationId;
            $localStorage.deviceType = ionic.Platform.isIOS() ? 'IOS' : 'ANDROID';
        }); 

        $rootScope.push.on('notification', function (data) {
            console.log(data);
            $rootScope.target = data.additionalData.state;
            if(data.additionalData.state === 'app.aviso')
            {
                $rootScope.targetParams = {'typeTab':data.additionalData.stateParams};
                if(data.additionalData.childId != null)
                {
                    $rootScope.data.selectedChild_photo = data.additionalData.childPhoto;
                    $rootScope.data.selectedChild_id = Number(data.additionalData.childId);
                    $localStorage.selectedChild_id =  Number(data.additionalData.childId);
                    $localStorage.selectedChild_photo = data.additionalData.childPhoto;
                }
            }
            else if(data.additionalData.state === 'app.calendar')
            {
                $rootScope.targetParams = {'currentDate':data.additionalData.stateParams};
            }

            if ($state.$current.name === 'preview' || $state.$current.name === 'home') 
            {
                
            } 
            else
            {
                $state.transitionTo($rootScope.target, $rootScope.targetParams, {
                    reload: true,
                    inherit: false,
                    notify: true
                });
            };
        });

        $rootScope.push.on('error', function (e) {
            $localStorage.testiTem = e.message;
        });
    });
})

.controller('AppCtrl', function($scope,$rootScope,$ionicSideMenuDelegate, $ionicPlatform,$location, $localStorage, $ionicHistory,$ionicLoading, $state, $ionicModal, Auth, ApiConfig, Family){

});
