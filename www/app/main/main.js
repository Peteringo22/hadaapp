angular.module( 'ep.main', [
    'ionic'
])


.controller('MainController', function($scope,$rootScope,$ionicSideMenuDelegate, $ionicPlatform,$location, $localStorage, $ionicHistory,$ionicLoading, $state, $stateParams, $ionicModal, Auth, ApiConfig, Family ){

    $scope.data ={
        notif:false
    }

    $rootScope.backImagesUrl=ApiConfig.get().baseUrl + '/public/images';
    $rootScope.backUrl=ApiConfig.get().baseUrl;
    $rootScope.data={
        myId:null,
        selectedChild_id:null,
        child:$localStorage.child,
        lastname:$localStorage.last_name
    };
    $scope.showAllChild = false;
    $rootScope.data.selectedChild_id=$localStorage.selectedChild_id;
    $rootScope.data.selectedChild_photo=$localStorage.selectedChild_photo;


    $rootScope.$watch(function () { return $localStorage.child; },function(){
        $rootScope.data.child=$localStorage.child;
    });

    $rootScope.$watch(function () { return $localStorage.last_name; },function(){
        $rootScope.data.lastname=$localStorage.last_name;
        $scope.data.lastname=$localStorage.last_name;
    });

    $rootScope.$watch(function () { return $localStorage.phone; },function(){
        $scope.data.phone=$localStorage.phone;
    });

    $rootScope.$watch(function () { return $localStorage.notif; },function(){
        console.log('CHANGE NOTIF');
        $scope.data.notif=$localStorage.notif;
        console.log($scope.data.notif);
    });


    $rootScope.$on('loading:hide', function (){
        $ionicLoading.hide();
    });
 
    $scope.isScheduled = function() {
        $cordovaLocalNotification.isScheduled("1234").then(function(isScheduled) {
            alert("Notification 1234 Scheduled: " + isScheduled);
        });
    }

    $scope.toggleLeft = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.logout = function() {
        Auth.logout();
    };

    $scope.OpenBubble = function(){
        var count=0;
        $rootScope.data.child=$localStorage.child;
         for (var i = 0; i < $rootScope.data.child.length; i++) {
            if($rootScope.data.child[i].id != $rootScope.data.selectedChild_id)
            {
                $rootScope.data.child[i].position = 90+80*count;
                count++;
            }
        };
        $scope.showAllChild = true;
    };

    $scope.CloseBubble = function(){
        $scope.showAllChild = false;

        if($state.current.name === 'app.aviso')
        {
            console.log('AVISO')
            $state.go($state.current, {'typeTab':'hijo'}, {reload: true});
        }
        else
        {
            $state.go($state.current, {}, {reload: true});
        }



    };

    $scope.OnSelectedChild = function(){
        if($scope.showAllChild)
        {
            $scope.CloseBubble();
            $scope.SelectChild($localStorage.selectedChild_id, $localStorage.selectedChild_photo);
        }
        else
        {
            $scope.OpenBubble();
        }
    };

    $scope.SelectChild = function(id, photo)
    {
        $localStorage.selectedChild_id = id;
        $localStorage.selectedChild_photo = photo;
        $rootScope.data.selectedChild_id=id;
        $rootScope.data.selectedChild_photo=photo;

        console.log($rootScope.data.hasRatingsLoaded());

        if($rootScope.data.hasRatingsLoaded()){
            $rootScope.data.loadGroupsAndPeriods();
        }

    }

    $scope.ChangeNotif = function()
    {
        if($localStorage.role == "family"){
            Family.SetNotif({'notif': $scope.data.notif, 'token': $localStorage.deviceToken}).$promise.then(function(data){
            });
        }
        else if($localStorage.role == "teacher"){
            Family.SetNotifT({'notif': $scope.data.notif, 'token': $localStorage.deviceToken}).$promise.then(function(data){
            });
        }
    }




});

