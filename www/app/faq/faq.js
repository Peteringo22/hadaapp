angular.module( 'ep.faq', [
	'ionic'
])

.controller('FaqCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, 
$ionicLoading, Faq,$ionicScrollDelegate, $timeout, $cordovaSocialSharing, ApiConfig, $ionicModal){

$rootScope.hasLeftMenu=true;
$rootScope.hasRightMenu=false;

	$scope.data={
			question:null,
            link: ApiConfig.get().baseUrl + '/public/images/faq'
		};

	$scope.$on('$ionicView.beforeEnter', function() {
		$scope.GetFaq();		
	});

	$scope.GetFaq = function()
	{
		Faq.GetAll().$promise.then(function(data){
            $scope.data.question=data.faq;
            for (var i=0; i<$scope.data.question.length; i++) {
                $scope.data.question[i].endDown = false;
                $scope.data.question[i].endUp = true;
                $scope.data.question[i].opened = false;
            };
            $ionicLoading.hide();
        });
	}

    $scope.isShowLike = function() {
        return $scope.showLike;
    };

    $scope.showQuestion = function(index)
    {
  	    $scope.data.question[index].endUp = false;
        $scope.data.question[index].endDown = false;
  	    $scope.data.question[index].opened = !$scope.data.question[index].opened;
        $timeout(function() {
            if($scope.data.question[index].opened===true)
            {
                $scope.data.question[index].endDown = true;
            }
            else
            {
                $scope.data.question[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 190);
    }

    $ionicModal.fromTemplateUrl('common/ui/popup-image.tpl.html', {
        id: '3',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal_photo = modal;
    });

        
    $scope.showImages = function(media) {
        $scope.data.image=$scope.data.link+'/'+media;
        $scope.data.nameImg = media;
        $scope.modal_photo.show();
    } 

    $scope.closeModal = function() {
        $scope.modal_photo.hide();
    };

    $scope.showPDF = function(url){
        window.open(url, '_system', 'location=yes'); 
        return false;
    }

});