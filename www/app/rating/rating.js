angular.module( 'ep.rating', [
	'ionic'
])

.controller('RatingCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, 
$ionicLoading, Rating , Group, Period, $ionicScrollDelegate, $timeout, $cordovaSocialSharing){

    $scope.data={
        /*child:null,
        rating:{matematicas:[10,9,6,8,5,7]},
        list_course:[],
        list_period:[],
        childSelected: {name: "boy", last_name: "boy"},
        listChild: ["1", "2"],*/
        groups: [],
        periods: [],
        ratings: [],
    };

    $scope.data.groupSelected = $scope.data.groups[0];
    $scope.data.periodSelected = $scope.data.periods[0];

    var monthNames = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
      "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"
    ];

    $rootScope.data.loadGroupsAndPeriods = function(){

        for(var i = 0; i < $localStorage.child.length; i++){
            if($localStorage.child[i].id == $localStorage.selectedChild_id){

                $scope.data.name = $localStorage.child[i].name;
                $scope.data.last_name = $localStorage.child[i].last_name;

            }
        }

        $scope.data.groups = [];
        $scope.data.periods = [];
        $scope.data.ratings = [];

        Group.getGroupsByChild({'id_child': $localStorage.selectedChild_id}).$promise.then(function(data){
            console.log(data);
            if(data.groups.length > 0 && data.success){

                for(var i = 0; i < data.groups.length; i++){
                    $scope.data.groups.push({
                        id : data.groups[i].id,
                        label : data.groups[i].grad + "-" + data.groups[i].group,
                        id_child_group: data.groups[i].child_group_id
                    });
                }

                $scope.data.groupSelected = $scope.data.groups[0];
                $scope.loadRatings($scope.data.groupSelected)

            }

        });

        Period.GetAllCurrentYear().$promise.then(function(data){
            if(data.period.length > 0 && data.success){
                console.log(data);
                for(var i = 0; i < data.period.length; i++){

                    var d = new Date(data.period[i].date_start);

                    $scope.data.periods.push({
                        id : data.period[i].id,
                        label : data.period[i].name
                    });
                }
            }

        });
    };

    $rootScope.data.loadGroupsAndPeriods();

    $rootScope.data.hasRatingsLoaded = function()
    { 
        return true;
    };

    $rootScope.data.hasRatingsLoaded();

    $scope.groupChanged = function()
    {
        $scope.loadRatings($scope.data.groupSelected);
    };

    $scope.periodChanged = function()
    {
        $scope.loadRatings($scope.data.groupSelected);
    };

    $scope.loadRatings = function(g_selected){
        console.log(g_selected);
        if(typeof g_selected != "undefined" && g_selected != null){

            $scope.data.ratings = [];

            Rating.GetRatingByCurrentYear({'id_child': $localStorage.selectedChild_id,
                                           'id_child_group' : g_selected.id_child_group})
                                            .$promise.then(function(data){
                                                console.log(data);
                if(data.success && data.ratings.length > 0){
                    for (var i = 0; i < data.teacher_courses.length; i++) {
                        $scope.data.ratings.push([
                            data.teacher_courses[i].name +
                            '<br><span> Profesor: ' + data.teacher_courses[i].t_name +
                            " " + data.teacher_courses[i].last_name + "</span>"
                            ]);

                        for(var j = 0; j < data.ratings.length; j++){

                            if(data.teacher_courses[i].id_course == 
                                data.ratings[j].id_course && data.ratings[j].rating_value != 0)
                                $scope.data.ratings[i].push(data.ratings[j].rating_value);

                        }
                        
                    }
                }

            });

        }
    };

    /*
    $scope.$on('$ionicView.beforeEnter', function() {
        $scope.loading = $ionicLoading.show({
           showBackdrop: false
        });
        $scope.data.oneChild =$localStorage.oneChild;
        $scope.loaded=false;
        $rootScope.hasLeftMenu=true;
        $rootScope.hasRightMenu=false;
        $scope.GetRating();
        $scope.GetChildInfos();  
        console.log($scope.data);    
    });

    $scope.GetRating= function()
    {
        $scope.noResult=false;
        Rating.GetRatingByChild({'id': $localStorage.selectedChild_id}).$promise.then(function(data){
             console.log(data);
            $ionicScrollDelegate.$getByHandle('horizontal').scrollTo(0,0);
 

            var promedio = [{course : 'Promedio' ,id_period:null,period:null, rating:null}];
            // Concatenate the new array onto the original
            data.list_course.Promedio = promedio;


            $scope.data.list_course=data.list_course;
            total =0;
            it = 0;

  
            angular.forEach(data.list_course, function(value, key) 
            {
                for (var i =0; i <  data.list_period.length; i++) 
                {     

                    if (angular.isUndefined($scope.data.list_course[key][i])) 
                    {
                        $scope.data.list_course[key].push({
                            id_period:data.list_period_id[i]
                        });
                    }

                    if ($scope.data.list_course[key][i].id_period != data.list_period_id[i] && key != 'Promedio') 
                    {
                        for (var z=0; z<data.list_course[key].length ; z++) 
                        {
                            if (angular.isDefined($scope.data.list_course[key][z].teacher_id))
                            {
                                index = z;
                                break;
                            };
                        };
                        console.log($scope.data.list_course[key])
                         $scope.data.list_course[key].unshift({
                            id_period:data.list_period_id[i],
                            teacher_id:$scope.data.list_course[key][index].teacher_id,
                            teacher_last_name:$scope.data.list_course[key][index].teacher_last_name,
                            teacher_name:$scope.data.list_course[key][index].teacher_name
                        });
                    }


                    for (var y=0; y<data.list_course[key].length ; y++) {
                        // console.log(key+' '+y);
                        // console.log($scope.data.list_course[key][y].rating);
                        if (angular.isUndefined($scope.data.list_course[key][y].rating)) 
                        {
                            $scope.data.list_course[key][y].rating= '-';
                        }
                    };
                };
            });


            for (var i =0; i <  data.list_period.length; i++) 
            {     
                total =0;
                it = 0;  
                angular.forEach(data.list_course, function(value, key) 
                {

                    console.log(!isNaN($scope.data.list_course[key][i].rating));

                    if(key != 'Promedio' && angular.isDefined($scope.data.list_course[key][i].rating) && !isNaN($scope.data.list_course[key][i].rating))
                    {
                        total = total + parseFloat($scope.data.list_course[key][i].rating);
                        it =  it+1;
                        console.log(total);
                    }
 
                    promedio = total/it;
                    // if (promedio === NaN) 
                    // {
                    //     promedio = '-';
                    // };

                    $scope.data.list_course.Promedio[i] = {course : 'Promedio' , period:data.list_period[i], rating:Math.round(promedio * 100) / 100};
                    // console.log($scope.data.list_course.Promedio[i].rating);

                    if (isNaN($scope.data.list_course.Promedio[i].rating)) 
                    {
                        $scope.data.list_course.Promedio[i].rating = '-';
                    };
                    // console.log($scope.data.list_course.Promedio[i].rating);
                });

            };


            angular.forEach( $scope.data.list_course, function(value, key) 
            {
                $scope.data.list_course[key].sort(function(a, b) {
                    return parseFloat(a.id_period) - parseFloat(b.id_period);
                });
            });
            // console.log($scope.data.list_course);

            $scope.data.list_period=data.list_period;
            $scope.data.list_comment=data.list_comment;
            $scope.noResult=data.noResult;
            $scope.lenghtRating = ($scope.data.list_period.length * 90);

            if (data.nbCourse == 0) 
            {
                $scope.noCourse =true;
            }
            else
            {
                $scope.noCourse =false;
            }

            if ($scope.noResult) 
            {
                $scope.lenghtRating= 80;
                if(data.nbCourse==0)
                {
                    $scope.heightNoResult=140; 
                }
                else
                {
                    $scope.heightNoResult=90 + (data.nbCourse) * 50;
                }
                
                $scope.data.list_period=[];
                $scope.noResult=true;
            };


            angular.forEach($scope.data.list_comment, function(value, key) {
                for (var y=0; y<$scope.data.list_comment[key].length; y++) {
                    $scope.data.list_comment[key][y].endDown = false;
                    $scope.data.list_comment[key][y].endUp = true;
                    $scope.data.list_comment[key][y].opened = false;
                };
            });
            $ionicLoading.hide();
            $scope.loaded=true; 
        });
    }

    $scope.GetChildInfos= function()
    {
        for (var y = $localStorage.child.length - 1; y >= 0; y--) {
            if ($localStorage.child[y].id === $localStorage.selectedChild_id) 
            {
                $scope.data.child=$localStorage.child[y];
            };
        };
    }

    $scope.GoTeacherProfile = function(id_teacher)
    {
        if (id_teacher != null) {
            $state.transitionTo("app.teacher-profile", {id:id_teacher}, {
                reload: true,
                inherit: false,
                notify: true
            });
        };
    }

    $scope.OpenCommentChild = function(index, icomment) {
        $scope.data.list_comment[index][icomment].endDown = false;
        $scope.data.list_comment[index][icomment].endUp = false;
        $scope.data.list_comment[index][icomment].opened = !$scope.data.list_comment[index][icomment].opened;
        $timeout(function() {
            if( $scope.data.list_comment[index][icomment].opened===true)
            {
                $scope.data.list_comment[index][icomment].endDown = true;
            }
            else
            {
                $scope.data.list_comment[index][icomment].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 20);
    };



    $timeout(function(){
        var sv = $ionicScrollDelegate.$getByHandle('horizontal').getScrollView();
        var container = sv.__container;
        var originaltouchStart = sv.touchStart;
        var originalmouseDown = sv.mouseDown;
        var originaltouchMove = sv.touchMove;
        var originalmouseMove = sv.mouseMove;

        container.removeEventListener('touchstart', sv.touchStart);
        container.removeEventListener('mousedown', sv.mouseDown);
        document.removeEventListener('touchmove', sv.touchMove);
        document.removeEventListener('mousemove', sv.mousemove);
        

        sv.touchStart = function(e) {
          e.preventDefault = function(){}
          originaltouchStart.apply(sv, [e]);
        }

        sv.touchMove = function(e) {
          e.preventDefault = function(){}
          originaltouchMove.apply(sv, [e]);
        }
        
        sv.mouseDown = function(e) {
          e.preventDefault = function(){}
          originalmouseDown.apply(sv, [e]);
        }

        sv.mouseMove = function(e) {
          e.preventDefault = function(){}
          originalmouseMove.apply(sv, [e]);
        }

        container.addEventListener("touchstart", sv.touchStart, false);
        container.addEventListener("mousedown", sv.mouseDown, false);
        document.addEventListener("touchmove", sv.touchMove, false);
        document.addEventListener("mousemove", sv.mouseMove, false);
    });
})
*/
/*.filter('reverse', function() {
    return function(items) {
    return items.slice().reverse();
  };*/
});

/*
(function() {
  var HorizontalScrollFix = (function() {
    function HorizontalScrollFix($timeout, $ionicScrollDelegate) {
      return {
        restrict: 'A',
        link: function(scope, element, attrs) {
          var mainScrollID = attrs.horizontalScrollFix,
              scrollID = attrs.delegateHandle;
          
          var getEventTouches = function(e) {
            return e.touches && (e.touches.length ? e.touches : [
              {
                pageX: e.pageX,
                pageY: e.pageY
              }
            ]);
          };
          
          var fixHorizontalAndVerticalScroll = function() {
            var mainScroll, scroll;
            mainScroll = $ionicScrollDelegate.$getByHandle(mainScrollID).getScrollView();
            scroll = $ionicScrollDelegate.$getByHandle(scrollID).getScrollView();
            
            // patch touchstart
            scroll.__container.removeEventListener('touchstart', scroll.touchStart);
            scroll.touchStart = function(e) {
              var startY;
              scroll.startCoordinates = ionic.tap.pointerCoord(e);
              if (ionic.tap.ignoreScrollStart(e)) {
                return;
              }
              scroll.__isDown = true;
              if (ionic.tap.containsOrIsTextInput(e.target) || e.target.tagName === 'SELECT') {
                scroll.__hasStarted = false;
                return;
              }
              scroll.__isSelectable = true;
              scroll.__enableScrollY = true;
              scroll.__hasStarted = true;
              scroll.doTouchStart(getEventTouches(e), e.timeStamp);
              startY = mainScroll.__scrollTop;
              
              $timeout((function() {
                var animate, yMovement;
                yMovement = startY - mainScroll.__scrollTop;
                if (scroll.__isDragging && yMovement < 2.0 && yMovement > -2.0) {
                  mainScroll.__isTracking = false;
                  mainScroll.doTouchEnd();
                  animate = false;
                  return mainScroll.scrollTo(0, startY, animate);
                } else {
                  return scroll.doTouchEnd();
                }
              }), 100);
            };
            scroll.__container.addEventListener('touchstart', scroll.touchStart);
          };
          $timeout(function() { fixHorizontalAndVerticalScroll(); });          
        }
      };
    }

    return HorizontalScrollFix;

  })();

  angular.module('ep').directive('horizontalScrollFix', ['$timeout', '$ionicScrollDelegate', HorizontalScrollFix]);

}).call(this);*/
