angular.module( 'ep.calendar', [
	'ionic'
])


.controller('CalendarCtrl', function($scope, $rootScope, $timeout, $ionicNavBarDelegate,$ionicLoading, $state, $stateParams, Event, EventDays, $localStorage,  $ionicScrollDelegate, $ionicModal){

	$scope.role = $localStorage.role;
	console.log("asdas");

    $scope.$on('$ionicView.beforeEnter', function() {
		$scope.data={
			dayEvents:[],
			pointerDays: EventDays
		};
    	if($stateParams.currentDate === 'now')
    	{
    		$scope.data.currentDate = new Date();
    	}
    	else
    	{
			$scope.data.currentDate = new Date($stateParams.currentDate);
			$scope.data.currentDate.setDate($scope.data.currentDate.getDate()+1);
    	}
    });

    $scope.$on('$ionicView.enter', function() {
        $rootScope.hasLeftMenu=true;
        $rootScope.hasRightMenu=false;
    });

    $scope.doRefresh = function() {
		$scope.getEventsByDate();
        $scope.$broadcast('scroll.refreshComplete');
    };
    
	$scope.getDayClass = function(date, mode) {
		if (mode === 'day') {
			var dayToCheck = new Date(date).setHours(0,0,0,0);

				for (var i=0;i<$scope.data.pointerDays.length;i++){
					var array=$scope.data.pointerDays[i].day.split("-");
					var currentDay = new Date();
					currentDay.setFullYear(array[0]);
					currentDay.setMonth(array[1]-1);
					currentDay.setDate(array[2]);
					currentDay=new Date(currentDay).setHours(0,0,0,0)
				if (dayToCheck === currentDay) {
					return 'event';
				}
			}
		}
		return '';
	};

	$scope.getDateFormat=function(date){
		return date.getFullYear()+"-"+(date.getMonth()+1)+"-"+date.getDate();
	};

	$scope.getEventsByDate=function(){
		$scope.loaded = false;
		$scope.data.dayEvents=[];

		if($scope.role == "family"){

			Event.getEventsByDay({'date': $scope.getDateFormat($scope.data.currentDate), 'id':$localStorage.id}).$promise.then(function(data){
				$scope.data.dayEvents=data.event;
				console.log(data);
				 for (var i=0; i<$scope.data.dayEvents.length; i++) {
	                $scope.data.dayEvents[i].endDown = false;
	                $scope.data.dayEvents[i].endUp = true;
	                $scope.data.dayEvents[i].opened = false;
	                $scope.data.dayEvents[i].time_end =  $scope.data.dayEvents[i].time_end.substring(0, 5);
	                $scope.data.dayEvents[i].time_start =  $scope.data.dayEvents[i].time_start.substring(0, 5);
	            };

				$ionicLoading.hide();
				$scope.loaded = true;
			});
		}
		else{
			Event.getEventsByDayT({'date': $scope.getDateFormat($scope.data.currentDate)}).$promise.then(function(data){
				console.log(data);
				$scope.data.dayEvents=data.event;
				 for (var i=0; i<$scope.data.dayEvents.length; i++) {
	                $scope.data.dayEvents[i].endDown = false;
	                $scope.data.dayEvents[i].endUp = true;
	                $scope.data.dayEvents[i].opened = false;
	                $scope.data.dayEvents[i].time_end =  $scope.data.dayEvents[i].time_end.substring(0, 5);
	                $scope.data.dayEvents[i].time_start =  $scope.data.dayEvents[i].time_start.substring(0, 5);
	            };
				$ionicLoading.hide();
				$scope.loaded = true;
			});
		}
	};

	$scope.$watch('data.currentDate', function(){
		if(angular.isDefined($scope.data.currentDate))
		{
			$scope.data.dayEvents=[];
			$ionicLoading.show({
	          showBackdrop: false
	        });
			$scope.getEventsByDate();
		}
	});

	$scope.formatAMPM=function(hour) {
		var array=hour.split(":");
		var hours = array[0] % 12;
		var minutes = array[1];
		var ampm = array[0] >= 12 ? 'pm' : 'am';
		hours = hours ? hours : 12; // the hour '0' should be '12'
		//minutes = minutes < 10 ? '0'+minutes : minutes;
		return hours + ':' + minutes + ' ' + ampm;
	}

    $scope.OpenEvent = function(index) {
        $scope.data.dayEvents[index].endDown = false;
        $scope.data.dayEvents[index].endUp = false;
        $scope.data.dayEvents[index].opened = !$scope.data.dayEvents[index].opened;
        $timeout(function() {
            if($scope.data.dayEvents[index].opened===true)
            {
                $scope.data.dayEvents[index].endDown = true;
            }
            else
            {
                $scope.data.dayEvents[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 200);
    };

    $ionicModal.fromTemplateUrl('common/ui/popup-image.tpl.html', {
        id: '3',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal_photo = modal;
    });

    $scope.showImages = function(media) {
        $scope.data.image="https://hadacomunica.com/gmback/laravel-backend/public/images/event/"+media;
        $scope.data.nameImg = media;
        $scope.modal_photo.show();
    } 

    $scope.closeModal = function() {
        $scope.modal_photo.hide();
    };

    $scope.showPDF = function(media){
        var url = "https://hadacomunica.com/gmback/laravel-backend/public/images/event/" + media;
        //url = url.replace("https", "http");
        url = url.replace("https", "http");
        //window.open = cordova.InAppBrowser.open;
        var ref = cordova.InAppBrowser.open(url, '_system', 'location=yes');
        //window.open(url, '_system', 'location=yes','toolbar=yes');  
        return false;
    }
});