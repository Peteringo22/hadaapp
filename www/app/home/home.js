angular.module( 'ep.home', [
	'ionic'
])

.controller('HomeCtrl', function($scope, $timeout,Family, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, $ionicLoading, Auth, $ionicPopup){
$rootScope.hasLeftMenu=false;
$rootScope.hasRightMenu=false;

	$scope.data={
			login: {
				key:null, 
				password:null}
			};

	$scope.$on('$ionicView.beforeEnter', function() {
		$ionicLoading.hide();
	});

	$scope.GoSelectChild = function()
	{
		$state.transitionTo("select-child", $stateParams, {
		    reload: true,
		    inherit: false,
		    notify: true
		});
	}

	$scope.Login = function(){
		$scope.ShowServerError=false;
		var error = false;
		if($scope.data.login.password === null || $scope.data.login.password === ""  )
		{ 
			$scope.serveurmessageSI = "Por favor de indicar tu contraseña";
			error=true;
			$scope.ShowServerError=true;
		}

		if($scope.data.login.key === null || $scope.data.login.key === "")
		{ 
			$scope.serveurmessageSI = "Por favor de indicar tu clave";
			error=true;
			$scope.ShowServerError=true;
		}

		if(!error)
		{
			$scope.ShowServerError=false;

			$scope.data.login.user = $scope.data.login.key;
			console.log($scope.data.login);

			Auth.login($scope.data.login).then(function(data){
				if(data.success)
				{
					$ionicLoading.hide();
					$scope.ShowServerError=false;
					$scope.data.login = {
					  email:null,
					  password:null
					};
				}
				else 
				{
					$ionicLoading.hide();
					$scope.ShowServerError=true;
					$scope.serveurmessageSI = data.message;
				}
			}, function() {
                $ionicLoading.hide();
                navigator.notification.alert(
                    'Intente de nuevo más tarde',  // message
                     $scope.GoHome(),         // callback
                    'Error de conexión',            // title
                    'Regresar'                  // buttonName
                );
            });
		}
	};

	$scope.OpenAlert = function(){
	   	var alertPopup = $ionicPopup.alert({
	     	title: '¿No tienes clave ?',
	     	template: 'Contacta a la escuela para obtener tu clave',
	     	cssClass:'alert',
	     	okType: 'button-balanced'
	   	});
	}

});