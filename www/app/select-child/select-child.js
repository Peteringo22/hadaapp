angular.module( 'ep.select-child', [
	'ionic'
])

.controller('SelectChildCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams){
	$rootScope.hasLeftMenu=false;
	$rootScope.hasRightMenu=false;

	$scope.data= {
		child:null,
		lastname:null,
		phone:$localStorage.phone
	}


	$scope.$on('$ionicView.beforeEnter', function() {
		$scope.GetChild();
	});


	$scope.GetChild = function()
	{
		$scope.data.child=$localStorage.child;
		$scope.data.lastname=$localStorage.last_name;
	}


	$scope.SelectChild = function(id, photo)
	{

		$localStorage.selectedChild_id = id;
		$localStorage.selectedChild_photo = photo;
		$rootScope.data.selectedChild_id=$localStorage.selectedChild_id;
    	$rootScope.data.selectedChild_photo=$localStorage.selectedChild_photo;

		$state.transitionTo("app.aviso", $stateParams, {
		    reload: true,
		    inherit: false,
		    notify: true
		});
	}


});