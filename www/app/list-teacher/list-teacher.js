angular.module( 'ep.list-teacher', [
	'ionic'
])

.controller('ListTeacherCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams,$ionicScrollDelegate,Teacher, $cordovaSocialSharing){


    $scope.$on('$ionicView.enter', function() {
        $rootScope.hasLeftMenu=true;
        $rootScope.hasRightMenu=false;
        $scope.GetListTeacher();
        $scope.data.oneChild =$localStorage.oneChild;
    });

    $scope.data =
    {
        teacher:null
    }

    $scope.GetListTeacher = function()
    {
        Teacher.GetTeacherByChild({'id': $localStorage.selectedChild_id}).$promise.then(function(data){
            console.log(data.teacher);
            $scope.data.teacher=data.teacher;
        });
    }

    $scope.GoTeacherProfile = function(id_teacher)
    {
        $state.transitionTo("app.teacher-profile", {id:id_teacher}, {
            reload: true,
            inherit: false,
            notify: true
        });
    }

    $scope.SendEmail = function(index)
    {
        console.log("hello");
        $cordovaSocialSharing
        .shareViaEmail(null, "Contact from EduPocket", $scope.data.teacher[index].email, null, null, null)
        .then(function(result) {

        }, function(err) {
            alert("Ocurrio un error al enviar tu correo");
        });
    }
});