angular.module( 'ep.preview', [
	'ionic'
])

.controller('PreviewCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, Auth){
    $rootScope.hasLeftMenu=false;
    $rootScope.hasRightMenu=false;
    $rootScope.appLoaded=true;


	  if($localStorage.token == undefined)
    {
       	$location.path("home");
    }
    else
    {
		    Auth.isConnected();
    }

});