angular.module( 'ep.aviso', [
    'ionic'
])

.controller('AvisoCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams,$ionicScrollDelegate,$ionicLoading, News,$localStorage, ApiConfig,  $cordovaFileTransfer, $ionicModal, $ionicTabsDelegate, $window){

    $scope.role = $localStorage.role;
    $rootScope.role = $localStorage.role;

    $scope.showChild = $rootScope.role == 'family' ? "ng-show" : "ng-hide";
    $scope.showTeacher = $rootScope.role == 'status' ? "ng-show" : "ng-hide";

    /*if ($stateParams.typeTab == 'hijo') 
    {
        $ionicTabsDelegate.select(0);
    } 
    else if($stateParams.typeTab == 'general')
    {
        $ionicTabsDelegate.select(1);
    }
    else
    {
        $ionicTabsDelegate.select(0);  
    }*/

    if($rootScope.role == 'family'){
        $ionicTabsDelegate.select(0);
    }
    else{
        $ionicTabsDelegate.select(1);
        //setTimeout(function(){ $ionicTabsDelegate.select(1); }, 1000);
    }

    $rootScope.hasLeftMenu=true;

    $scope.$on('$ionicView.enter', function() {
        $scope.loading = $ionicLoading.show({
           showBackdrop: false
        });
        $rootScope.hasLeftMenu=true;
        $rootScope.hasRightMenu=false;
        
        if($scope.role == "family")
            $scope.getAvisos();
        else
            $scope.getAvisosTeacher();

        /*if ($stateParams.typeTab == 'hijo') 
        {
            $ionicTabsDelegate.select(0);
        } 
        else if($stateParams.typeTab == 'general')
        {
            $ionicTabsDelegate.select(1);
        }
        else
        {
            $ionicTabsDelegate.select(0);  
        }
        $scope.setTabView();*/
        if($rootScope.role == 'family'){
            $ionicTabsDelegate.select(0);
        }
        else{
            $ionicTabsDelegate.select(1);
            //setTimeout(function(){ $ionicTabsDelegate.select(1); }, 1000);
        }
        $scope.data.selectedChild_id =$localStorage.selectedChild_id;
        $scope.data.oneChild =$localStorage.oneChild;


        if ($scope.data.oneChild) {
            $scope.data.titleHijo ='MI HIJO';
        }
        else
        {
            $scope.data.titleHijo ='MIS HIJOS';
        }
    });



    $scope.data={
        avisoGeneral:null,
        avisoChild:null,
        link: ApiConfig.get().baseUrl + '/public/images/aviso',
    }



    $scope.setTabView = function() {
        // console.log($stateParams);
        // if ($stateParams.typeTab == 'hijo') 
        // {
        //     $ionicTabsDelegate.select(0);
        // } 
        // else if($stateParams.typeTab == 'general')
        // {
        //     $ionicTabsDelegate.select(1);
        // }
        // else
        // {
        //     $ionicTabsDelegate.select(0);  
        // }
    };

    $scope.doRefresh = function() {
        News.GetByFamily({'id': $localStorage.id}).$promise.then(function(data){

            $scope.data.avisoGeneral=data.general;
            for (var i=0; i<$scope.data.avisoGeneral.length; i++) {
                $scope.data.avisoGeneral[i].endDown = false;
                $scope.data.avisoGeneral[i].endUp = true;
                $scope.data.avisoGeneral[i].opened = false;
            };
            
            $scope.data.avisoChild=data.avisoChild;
            for (var i=0; i<$scope.data.avisoChild.length; i++) {
                for (var y=0; y<$scope.data.avisoChild[i].aviso.length; y++) {
                    $scope.data.avisoChild[i].aviso[y].endDown = false;
                    $scope.data.avisoChild[i].aviso[y].endUp = true;
                    $scope.data.avisoChild[i].aviso[y].opened = false;
                };
            };

            $scope.$broadcast('scroll.refreshComplete');
        });
    };


    /*
    * if given group is the selected group, deselect it
    * else, select the given group
    */
    $scope.title= '<i class="icon ion-android-textsms icone-menu white"></i>';
    $scope.OpenAviso = function(index) {

        $scope.groups[index].endDown = false;
        $scope.groups[index].endUp = false;
        $scope.groups[index].opened = !$scope.groups[index].opened;
        $timeout(function() {
            if($scope.groups[index].opened===true)
            {
                $scope.groups[index].endDown = true;
            }
            else
            {
                $scope.groups[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 200);
    };

    $scope.OpenAvisoGeneral = function(index) {
        $scope.data.avisoGeneral[index].endDown = false;
        $scope.data.avisoGeneral[index].endUp = false;
        $scope.data.avisoGeneral[index].opened = !$scope.data.avisoGeneral[index].opened;
        $timeout(function() {
            if($scope.data.avisoGeneral[index].opened===true)
            {
                $scope.data.avisoGeneral[index].endDown = true;
            }
            else
            {
                $scope.data.avisoGeneral[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 20);
    };

    $scope.OpenAvisoTeacherG = function(index) {
        $scope.data.avisoChild[index].endDown = false;
        $scope.data.avisoChild[index].endUp = false;
        $scope.data.avisoChild[index].opened = !$scope.data.avisoChild[index].opened;
        $timeout(function() {
            if($scope.data.avisoChild[index].opened===true)
            {
                $scope.data.avisoChild[index].endDown = true;
            }
            else
            {
                $scope.data.avisoChild[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 20);
    };

    $scope.OpenAvisoChild = function(index, iaviso) {
        console.log(index, iaviso);
        $scope.data.avisoChild[index].aviso[iaviso].endDown = false;
        $scope.data.avisoChild[index].aviso[iaviso].endUp = false;
        $scope.data.avisoChild[index].aviso[iaviso].opened = !$scope.data.avisoChild[index].aviso[iaviso].opened;
        $timeout(function() {
            if( $scope.data.avisoChild[index].aviso[iaviso].opened===true)
            {
                $scope.data.avisoChild[index].aviso[iaviso].endDown = true;
            }
            else
            {
                $scope.data.avisoChild[index].aviso[iaviso].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 20);
    };

    $scope.getAvisos = function(index) {

        News.GetByFamily({'id': $localStorage.id}).$promise.then(function(data){
            
            console.log("todos avisos");
            console.log(data);
            $scope.data.avisoGeneral=data.general;

            if(data.general.length > 0){
                console.log("avisos");
                console.log(data);

                for (var i=0; i<$scope.data.avisoGeneral.length; i++) {
                    $scope.data.avisoGeneral[i].endDown = false;
                    $scope.data.avisoGeneral[i].endUp = true;
                    $scope.data.avisoGeneral[i].opened = false;
                };
            }

            $scope.data.avisoChild=data.avisoChild;
            console.log($scope.data.avisoChild);

            if(data.avisoChild.length > 0){
                for (var i=0; i<$scope.data.avisoChild.length; i++) {

                    if(typeof $scope.data.avisoChild[i].aviso != 'undefined'){
                        for (var y=0; y<$scope.data.avisoChild[i].aviso.length; y++) {
                            $scope.data.avisoChild[i].aviso[y].endDown = false;
                            $scope.data.avisoChild[i].aviso[y].endUp = true;
                            $scope.data.avisoChild[i].aviso[y].opened = false;
                        };
                    }
                };
            }


            $ionicLoading.hide();
        });

    };

    $scope.getAvisosTeacher = function(index) {
        News.GetByTeacher({'id': $localStorage.id_teacher}).$promise.then(function(data){

            $scope.data.avisoGeneral=data.general;

            if(typeof data.general != 'undefined' ){

                for (var i=0; i<$scope.data.avisoGeneral.length; i++) {
                    $scope.data.avisoGeneral[i].endDown = false;
                    $scope.data.avisoGeneral[i].endUp = true;
                    $scope.data.avisoGeneral[i].opened = false;
                };
            }

            console.log(data);

            $scope.data.avisoChild=data.avisoChild;

            if(typeof data.avisoChild != 'undefined'){

                for (var i=0; i<$scope.data.avisoChild.length; i++) {
                    $scope.data.avisoChild[i].endDown = false;
                    $scope.data.avisoChild[i].endUp = true;
                    $scope.data.avisoChild[i].opened = false;
                };
            }

            $ionicLoading.hide();
        });
    };

// $scope.download = function(media) {
//     $scope.url="http://papers.com.pk/xads/2015/8-10/Express/1103007127-1.jpg";
//     var filename = $scope.url.split("/").pop();
//     alert(filename);
//     window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {
//     fs.root.getDirectory(
//     "papers",
//         {   
//             create: true
//         },
//         function(dirEntry) {
//             dirEntry.getFile(
//                 filename, 
//                 {

//                     create: true, 
//                     exclusive: false
//                 }, 
//                 function gotFileEntry(fe) {
//                     var p = fe.toURL();
//                     fe.remove();
//                     ft = new FileTransfer();
//                     ft.download(
//                         encodeURI($scope.url),
//                         p,
//                         function(entry) {
//                            //Pace.restart();
//                            console.log(entry);

//                             $scope.imgFile = entry.toURL();
//                         },
//                         function(error) {
                            
//                             alert("Download Error Source -> " + error.source);
//                         },
//                        false
//                     );
//                 }, 
//                 function() {
//                     $ionicLoading.hide();
//                     console.log("Get file failed");
//                 }
//             );
//         }
//     );
// },
// function() {
//     $ionicLoading.hide();
//     console.log("Request for filesystem failed");
// });

// }



    $scope.downloadFunc = function(media){

        var url = "https://www.mathworks.com/moler/random.pdf";
        var targetPath = cordova.file.documentsDirectory + "test.pdf";
        var trustHosts = true;
        var options = {};

        $cordovaFileTransfer.download(url, targetPath, options, trustHosts)
          .then(function(result) {
            // Success!
        }, function(err) {
            // Error
        }, function (progress) {
            $timeout(function () {
              $scope.downloadProgress = (progress.loaded / progress.total) * 100;
            });
        });

        
    };


    $ionicModal.fromTemplateUrl('common/ui/popup-image.tpl.html', {
        id: '3',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal_photo = modal;
    });

    $scope.showImages = function(media) {
        $scope.data.image="https://hadacomunica.com/pruebalaravel/laravel-backend/public/images/aviso/"+media;
        $scope.data.nameImg = media;
        $scope.modal_photo.show();
    } 

    $scope.closeModal = function() {
        $scope.modal_photo.hide();
    };

    $scope.showPDF = function(media){
        //var url = "http://en.unesco.org/inclusivepolicylab/sites/default/files/dummy-pdf_2.pdf";
        var url = "https://hadacomunica.com/gmback/laravel-backend/public/images/aviso/" + media;
        //media = media.replace("https", "http");
        window.open(url, '_system', 'location=yes'); 
        //return false;*/
        //var url = "https://hadacomunica.com/pruebalaravel/laravel-backend/public/images/aviso/sw7_XOKEv.pdf";
        //url = url.replace("https", "http");
        //window.open = cordova.InAppBrowser.open;
        //console.log(cordova.InAppBrowser);
        //var ref = cordova.InAppBrowser.open(url, '_system');
        //window.open(url, '_system', 'location=yes','toolbar=yes');  
        //return false;
    }


    
    /*$window.openLink = function(link) {
    console.log("click en link");
       testfam console.log(link);
    window.open( link, '_system');
    };*/

});
