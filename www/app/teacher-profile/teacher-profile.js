angular.module( 'ep.teacher-profile', [
	'ionic'
])

.controller('TeacherProfileCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, 
$ionicLoading, Teacher,$ionicScrollDelegate, $timeout, $cordovaSocialSharing, $ionicHistory){

	$scope.data={
		teacher:null,
		id_teacher:$stateParams.id,
		cv:null
	};

	$scope.$on('$ionicView.beforeEnter', function() {
	    $rootScope.hasLeftMenu=false;
        $rootScope.hasRightMenu=false;	
		$scope.data.id_teacher =  $stateParams.id;
		$scope.GetProfileTeacher();	
	});

	$scope.GetProfileTeacher = function()
	{
		Teacher.GetProfileTeacher({'id': $scope.data.id_teacher}).$promise.then(function(data){
            console.log(data);
            $scope.data.teacher=data.teacher;
            for (var i =  data.cv.length - 1; i >= 0; i--) {
                data.cv[i].endUp = false;
                data.cv[i].endDown = false;
                data.cv[i].opened = false;
            };
            $scope.data.cv=data.cv;
            $ionicLoading.hide();
        });
	}

	$scope.GoBack = function()
    {
        $ionicHistory.goBack(-1);
    }

    $scope.SendEmail = function()
    {
         $cordovaSocialSharing.shareViaEmail(null, "Contact from EduPocket", $scope.data.teacher.email, null, null, null)
        .then(function(result) {

        }, function(err) {
            alert("Ocurrio un error al enviar tu correo");
        });
    }


	$scope.showLike = false;
    $scope.endLikeUp = true;
    $scope.endLikeDown = true;
    $scope.endTravelUp = true;
    $scope.endTravelDown = true;
    $scope.endActividadesUp = true;
    $scope.endActividadesDown = true;
    $scope.endInfosUp = true;
    $scope.endInfosDown = true;
    $scope.showTravel = false;
    $scope.showActividades = false;
    $scope.showInfos = false;

    $scope.OpenCv = function(index) {
        $scope.data.cv[index].endDown = false;
        $scope.data.cv[index].endUp = false;
        $scope.data.cv[index].opened = !$scope.data.cv[index].opened;
        $timeout(function() {
            if($scope.data.cv[index].opened===true)
            {
                $scope.data.cv[index].endDown = true;
            }
            else
            {
                $scope.data.cv[index].endUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 20);
    };

    $scope.isShowLike = function() {
        return $scope.showLike;
    };

    $scope.showLike = function()
    {
  	    $scope.endLikeUp = false;
        $scope.endLikeDown = false;
  	    $scope.data.showLike = !$scope.data.showLike;
        $timeout(function() {
            if($scope.data.showLike===true)
            {
                $scope.endLikeDown = true;
            }
            else
            {
                $scope.endLikeUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 190);
    }

    $scope.isShowTravel = function() {
        return $scope.showTravel;
    };

    $scope.showTravel = function()
    {
        $scope.endTravelUp = false;
        $scope.endTravelDown = false;
        $scope.data.showTravel = !$scope.data.showTravel;
        $timeout(function() {
            if($scope.data.showTravel===true)
            {
                $scope.endTravelDown = true;
            }
            else
            {
                $scope.endTravelUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 190);
    }

    $scope.isShowActividades = function() {
        return $scope.showActividades;
    };

    $scope.showActividades = function()
    {
        $scope.endActividadesUp = false;
        $scope.endActividadesDown = false;
        $scope.data.showActividades = !$scope.data.showActividades;
        $timeout(function() {
            if($scope.data.showActividades===true)
            {
                $scope.endActividadesDown = true;
            }
            else
            {
                $scope.endActividadesUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 190);
    }


    $scope.isShowInfos = function() {
        return $scope.showInfos;
    };

    $scope.showInfos = function()
    {       
        $scope.endInfosUp = false;
        $scope.endInfosDown = false;
        $scope.data.showInfos = !$scope.data.showInfos;
        $timeout(function() {
            if($scope.data.showInfos===true)
            {
                $scope.endInfosDown = true;
            }
            else
            {
                $scope.endInfosUp = true;
            }
            $ionicScrollDelegate.resize();
        }, 190);
    }




});