angular.module( 'ep.school', [
	'ionic'
])

.controller('SchoolCtrl', function($scope, $timeout, $location, $ionicNavBarDelegate,  $rootScope, $localStorage,$state, $stateParams, $ionicSideMenuDelegate,
$ionicLoading, School ,$ionicScrollDelegate, $timeout, $cordovaSocialSharing, $cordovaSocialSharing, $ionicModal,  $timeout ){


    $rootScope.slideHeader = false;
    $rootScope.slideHeaderPrevious = 0;
	$scope.data={
			question:null
		};

	$scope.$on('$ionicView.beforeEnter', function() {
		$scope.GetSchool();	
        $rootScope.hasLeftMenu=true;
        $rootScope.hasRightMenu=false;	
	});

	$scope.GetSchool = function()
	{
		School.GetSchool().$promise.then(function(data){
            $scope.data.school=data.school;
            $ionicLoading.hide();
        });
	}

    $scope.data.fixeTabs=230;
  
    $scope.getScrollPosition = function() {

        $timeout(function () {
            $scope.data.scroll = $ionicScrollDelegate.$getByHandle('test').getScrollPosition();
            if($scope.data.scroll.top > 230)
            {
                $scope.data.fixeTabs = $scope.data.scroll.top;
                $scope.data.fixe = true;
            }
            else
            {
                $scope.data.fixeTabs = 230;
                $scope.data.fixe = false;
            }
        });
    };
   

    $scope.data.idxTab=1;
    $scope.changeTabs = function(idx) {
        $scope.data.idxTab = idx;
        $ionicScrollDelegate.resize();
        if($scope.data.fixe)
        {
            $ionicScrollDelegate.scrollTo(0,231);
        }     
    };

    $scope.OpenMenu = function()
    {
        $ionicSideMenuDelegate.toggleLeft();
    }



    $scope.Compartir = function()
    {
        $cordovaSocialSharing.share(null,  "Share from EduPocket", null, $scope.data.school.pdf_url) // Share via native share sheet
        .then(function(result) {
          // Success!
        }, function(err) {
          // An error occured. Show a message to the user
        });
    }



    $ionicModal.fromTemplateUrl('common/ui/popup-video.tpl.html', {
        id: '4',
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modal_video = modal;
    });


   // $scope.videoUrl="http://player.vimeo.com/external/85569724.sd.mp4?s=43df5df0d733011263687d20a47557e4";
    $scope.videoUrl="http://www.youtube.com/embed/5OSHFV_N11g";

    $scope.playVideo = function() {
        $scope.modal_video.show();
        $scope.play()
    }

  // Close the modal
    $scope.closeModal = function() {
        $scope.pause();
        $scope.modal_video.hide();
    };


    $scope.pause = function(){
        var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
        iframe.postMessage('{"event":"command","func":"' + 'pauseVideo' +   '","args":""}', '*');
    }

    $scope.play = function(){
        $timeout(function() 
        {
            var iframe = document.getElementsByTagName("iframe")[0].contentWindow;
            iframe.postMessage('{"event":"command","func":"' + 'playVideo' +   '","args":""}', '*');
        }, 1500);
    }

})

.filter("trustUrl", ['$sce', function ($sce) {
    return function (recordingUrl) {
        return $sce.trustAsResourceUrl(recordingUrl);
    };
}]);